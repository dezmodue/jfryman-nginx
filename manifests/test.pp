class { 'nginx': }

nginx::resource::vhost { 'www.test.com':
  www_root => '/tmp/www.test.com',
}
