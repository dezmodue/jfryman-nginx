# Encoding: utf-8
require 'serverspec'

set :backend, :exec
set :path, '/sbin:/usr/local/sbin:$PATH'

describe command('curl localhost') do
  its(:stdout) { should match /404.*Found/ }
end

describe process("nginx") do
  its(:user) { should eq "root" }
  its(:args) { should match /-c \/etc\/nginx\/nginx.conf/ }
end

describe port(80) do
  it { should be_listening }
end

describe package('nginx') do
  it { should be_installed }
end

#describe command('ls /foo') do
#  its(:stderr) { should match /No such file or directory/ }
#end

#describe command('ls /foo') do
#  its(:exit_status) { should eq 0 }
#end
